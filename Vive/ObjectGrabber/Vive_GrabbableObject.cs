﻿using UnityEngine;
using System.Collections;

namespace Libonati.VR{
	public class Vive_GrabbableObject : MonoBehaviour {

		private Transform tracker;

		private Rigidbody rigid;
		public Rigidbody rigidBody {
			get {
				return rigid;
			}
		}
		protected Vive_ObjectGrabber grabber;
		private bool highlighting = false;
		public Vive_ObjectGrabber currentGrabber{
			get{
				return grabber;
			}
		}
		public bool dragging{
			get{
				return grabber != null;
			}
		}

		public delegate void OnGrabEvent(Vive_GrabbableObject obj);
		public event OnGrabEvent onGrabStart;
		public event OnGrabEvent onGrab;
		public event OnGrabEvent onGrabStop;

		private bool usingGravity = false;

		public bool canGrab = true;
		public bool canScale = false;

		virtual protected void Awake(){
			rigid = gameObject.GetComponent<Rigidbody> ();
			if (rigid == null) {
				rigid = gameObject.AddComponent<Rigidbody> ();
			}
			tracker = new GameObject ("GrabbableTracker").transform;
			tracker.transform.parent = transform;
			LibonatiHelper.resetTransform (tracker);
		}
		virtual protected void Update(){
			if (Vive_Controller.Instance == null) {
				return;
			}
		}

		//Detect when the hand is around us
		protected virtual void OnTriggerEnter(Collider collider){
			if (dragging) {
				return;
			}
			Vive_ObjectGrabber grabber = collider.gameObject.GetComponent<Vive_ObjectGrabber> ();
			if (grabber != null) {
				if (grabber.setFocus ((Vive_GrabbableObject)this)) {
					//We are now the focus
					startHighlight();
				}
			}
		}
		protected virtual void OnTriggerExit(Collider collider){
			if (dragging) {
				return;
			}
			Vive_ObjectGrabber grabber = collider.gameObject.GetComponent<Vive_ObjectGrabber> ();
			if (grabber != null) {
				grabber.releaseFocus((Vive_GrabbableObject)this);
				stopHighlight ();
			}
		}

		virtual public void startDrag(Vive_ObjectGrabber newGrabber, Transform newParent){
			if (dragging) {
				grabber.stopGrab ();
			}
			grabber = newGrabber;
			if (onGrabStart != null) {
				onGrabStart ((Vive_GrabbableObject)this);
			}
			usingGravity = rigid.useGravity;
			rigid.useGravity = false;
			tracker.parent = newParent;
			startHighlight ();
		}
		virtual public void drag(){
			if (onGrab != null) {
				onGrab ((Vive_GrabbableObject)this);
			}
			rigid.MovePosition (tracker.position);
			rigid.MoveRotation (tracker.rotation);
			rigid.velocity = grabber.myController.velocity;
			rigid.angularVelocity = grabber.myController.angularVelocity;
		}
		virtual public void stopDrag(Vector3 vel, Vector3 torque){
			grabber = null;
			if (onGrabStop != null) {
				onGrabStop ((Vive_GrabbableObject)this);
			}
			tracker.parent = transform;
			LibonatiHelper.resetTransform (tracker);
			rigid.useGravity = usingGravity;
			stopHighlight ();
		}
		virtual protected void startHighlight(){
			
		}
		virtual protected void stopHighlight(){

		}

		public void forceRelease(){
			if (grabber != null) {
				grabber.stopGrab ();
			}
		}
	}
}