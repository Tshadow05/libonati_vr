﻿using UnityEngine;
using System.Collections;
using Valve.VR;

namespace Libonati.VR{
	[RequireComponent(typeof(SphereCollider), typeof(Rigidbody))]
	public class Vive_ObjectGrabber : Vive_Component {
		private enum GrabState{
			NONE,
			GRAB,
			SCALE
		}
		private GrabState grabState = GrabState.NONE;

		private bool grabbing{
			get{
				return grabState != GrabState.NONE;
			}
		}

		private Vive_GrabbableObject currentObject;
		public SteamVR_Controller.Device myController {
			get {
				if (rightHand) {
					return vive.hand_R.data;
				} else {
					return vive.hand_L.data;
				}
			}
		}

		private Vive_ObjectGrabber partnerGrabber;

		//Scaleing
		private Vector3 scaleStart;
		private Vector3 scaleStartRotation;
		private Vector3 scaleStartDistance;
		private Vector3 scaleCurrentVector;
		private Transform scaleTransform;


		public Transform handT{
			get{
				if (rightHand) {
					return vive.hand_R.transform;
				} else {
					return vive.hand_L.transform;
				}
			}
		}

		public float grabRange{
			get{
				return sphereCollider.radius;
			}
		}

		private Rigidbody rigid;
		private SphereCollider sphereCollider;
		private Vive_GrabbableObject currentFocus;

		public bool rightHand = false;
		public bool scaleEnabled = false;
		public float strengthModifier = 1;


		private Vector3 partnerDistance{
			get{
				return partnerGrabber.position-position;
			}
		}
		private Vector3 scaleStartVector {
			get {
				return (scaleCurrentVector.normalized * scaleStartDistance.magnitude);
			}
		}
		public Vector3 position{
			get{
				return handT.position;
			}
		}
			
		protected override void Awake(){
			base.Awake ();
			sphereCollider = gameObject.GetComponent<SphereCollider> ();
		}

		protected override void onFinishedLoading ()
		{
			base.onFinishedLoading ();

			vive.onControllerButtonDown += onButtonDown;
			vive.onControllerButtonUp += onButtonUp;
			vive.onControllerButton += onButton;

			scaleTransform = new GameObject ("ScaleTransform").transform;
			scaleTransform.parent = transform;
		}

		//When a grabbable object gets in range it notifies use through this
		public bool setFocus(Vive_GrabbableObject obj){
			if (!obj.canGrab && !obj.canScale) {
				return false;
			}
			//Check if we already have a better focus target
			if (currentFocus != null) {
				float oldDistance = Vector3.Distance(transform.position, currentFocus.transform.position);
				float newDistance = Vector3.Distance(transform.position, obj.transform.position);
				if (newDistance > oldDistance) {
					return false;
				}
			}
			//Set our new target
			currentFocus = obj;
			//Rumble
			myController.TriggerHapticPulse(500);
			return true;
		}
		public bool releaseFocus(Vive_GrabbableObject obj){
			//if it is our current focus
			if (currentFocus == obj) {
				//clear focus
				currentFocus = null;
				//Rumble
				myController.TriggerHapticPulse(500);
				return true;
			}
			return false;
		}

		void onButtonDown(EVRButtonId buttonId, SteamVR_Controller.Device controller){
			if (controller != myController || buttonId != EVRButtonId.k_EButton_SteamVR_Trigger) {
				return;
			}
			if (currentFocus != null) {
				startGrab (currentFocus);
			}
		}
		void onButton(EVRButtonId buttonId, SteamVR_Controller.Device controller){
			if (controller != myController || buttonId != EVRButtonId.k_EButton_SteamVR_Trigger) {
				return;
			}
			updateGrab ();
		}
		void onButtonUp(EVRButtonId buttonId, SteamVR_Controller.Device controller){
			if (controller != myController || buttonId != EVRButtonId.k_EButton_SteamVR_Trigger) {
				return;
			}
			stopGrab ();
		}



		public void startGrab(Vive_GrabbableObject obj){
			//Make sure we don't grab two things at once
			if (currentObject != null && grabState == GrabState.GRAB) {
				currentObject.stopDrag (Vector3.zero, Vector3.zero);
			}

			currentObject = obj;

			//Check if the object is already being grabbed by a controller
			if (scaleEnabled && currentObject.canScale && currentObject.dragging) {
				//Start Scale
				grabState = GrabState.SCALE;
				partnerGrabber = currentObject.currentGrabber;

				scaleTransform.rotation = Quaternion.identity;
				scaleTransform.localScale = Vector3.one;

				scaleStart = currentObject.transform.localScale;
				scaleStartRotation = currentObject.transform.eulerAngles + new Vector3 (180, 180, 180);
				scaleStartDistance = partnerDistance;
				scaleTransform.position = partnerGrabber.position;
				scaleTransform.LookAt (position);
				currentObject.transform.SetParent (scaleTransform, true);
			} else if (obj.canGrab) {
				//Start Grab
				grabState = GrabState.GRAB;
				currentObject.startDrag ((Vive_ObjectGrabber)this, handT);
			} else {
				currentObject = null;
				return;
			}

		}
		private void updateGrab(){
			if (currentObject != null) {
				if (grabState == GrabState.GRAB) {
					//Update Drag
					currentObject.drag ();
				} else if (grabState == GrabState.SCALE) {
					//Check if the partner stopped grabbing
					if (partnerGrabber.grabState == GrabState.GRAB) {
						//Update Scale
						scaleCurrentVector = partnerDistance;
						scaleTransform.position = partnerGrabber.position;
						scaleTransform.LookAt (position);
						scaleTransform.localScale = Vector3.one *  (1+ (scaleCurrentVector.magnitude-scaleStartDistance.magnitude) * 2);

					} else {
						//partner stopped grabbing
						startGrab(currentObject);
					}
				}
			}
		}
		public void stopGrab(){
			if (currentObject != null) {
				if (grabState == GrabState.GRAB) {
					//End Grab
					currentObject.stopDrag (myController.velocity * strengthModifier, myController.angularVelocity);
				}else if (grabState == GrabState.SCALE){
					//End Scale
					currentObject.transform.SetParent(partnerGrabber.handT,true);
				}
				currentObject = null;
				grabState = GrabState.NONE;
			}
		}

		void OnDrawGizmos(){
			if (grabState == GrabState.GRAB) {
				Gizmos.color = Color.red;
				Gizmos.DrawSphere (position, .1f);
			}else if (grabState == GrabState.SCALE) {
				Gizmos.color = Color.cyan;
				Gizmos.DrawSphere (position, .1f);
				Gizmos.color = Color.red;
				Gizmos.DrawLine (position, position + scaleCurrentVector);
				Gizmos.color = Color.blue;
				Gizmos.DrawLine (position + new Vector3(0,.03f,.03f), position + scaleStartVector + new Vector3(0,.03f,.03f));
			}
		}
	}
}