﻿using UnityEngine;
using System.Collections;


namespace Libonati.VR{
	public class Vive_GrabbableSnowglobe : Vive_GrabbableObject {
		//Inner graphics
		public GameObject innerGraphicTemplate;
		private GameObject innerGraphic;
		public Transform innerGraphicHolder;

		//Highlighting
		public GameObject highlight;
		public MeshRenderer highlightColorRenderer;
		private Material highlightMat;
		public Color highlightColor;
		private Color defaultColor;

		// Use this for initialization
		override protected void Awake () {
			base.Awake ();
			//innerGraphicHolder = transform.GetChild (0);
			innerGraphic = innerGraphicHolder.GetChild (0).gameObject;

			if (innerGraphicTemplate != null) {
				addInnerGraphic ((GameObject)Instantiate (innerGraphicTemplate));	
			}

			//highlight = transform.GetChild (1).gameObject;
			highlightMat = highlightColorRenderer.material;
			defaultColor = highlightMat.GetColor("_TintColor");

			stopHighlight ();
		}
		
		// Update is called once per frame
		override protected void Update () {
			if (Vive_Controller.Instance == null) {
				return;
			}
			base.Update ();
		}

		public void addInnerGraphic(GameObject newGraphic){
			Destroy (innerGraphic);
			innerGraphic = newGraphic;
			innerGraphic.transform.SetParent (innerGraphicHolder, false);
			LibonatiHelper.resetTransform (innerGraphic.transform);
		}

		protected override void startHighlight(){
			if (highlight != null) {
				highlight.SetActive (true);
			}
			if (highlightMat != null) {
				highlightMat.SetColor ("_TintColor", highlightColor);
			}
		}
		protected override void stopHighlight(){
			if (highlight != null) {
				highlight.SetActive (false);
			}
			if (highlightMat != null) {
				highlightMat.SetColor ("_TintColor", defaultColor);
			}
		}
	}
}