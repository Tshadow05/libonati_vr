﻿using UnityEngine;
using System.Collections;

namespace Libonati.VR{
	public class Vive_Component : MonoBehaviour {
		[HideInInspector]
		public Vive_Controller vive;

		// Use this for initialization
		protected virtual void Awake () {
			vive = Vive_Controller.Instance;
			vive.addOnFinishedLoadingListener(onFinishedLoading);
		}
		protected virtual void onFinishedLoading(){

		}
	}
}
