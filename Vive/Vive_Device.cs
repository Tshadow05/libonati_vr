﻿using UnityEngine;
using System.Collections;

namespace Libonati.VR{
	public class Vive_Device{

		[HideInInspector]
		public SteamVR_TrackedObject trackedObject;
		[HideInInspector]
		public Vive_Controller controller;

		public int id {
			get {
				return (int)trackedObject.index;
			}
		}
		//Steam VR DEVICE DATA
		private SteamVR_Controller.Device deviceData = null;
		public SteamVR_Controller.Device data{
			get{
				if (id < 0) {
					return null;
				}else if(deviceData == null){
					try{
						deviceData = SteamVR_Controller.Input (id);
					}catch{}
				}
				return deviceData;
			}
		}

		//The CONTROLLERS POSITIONING
		public Transform transform{
			get{
				return trackedObject.transform;
			}
		}

		public Vector3 position{
			get{
				if (transform == null) {
					return Vector3.zero;
				}
				return transform.position;
			}
		}

		public Vector3 positionLerp{
			get{
				if (data == null) {
					return position;
				}
				return controller.transform.TransformPoint(data.transform.pos);
			}
		}
	}
}