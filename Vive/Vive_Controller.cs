﻿//#define LOG_DEBUG //Comment this to stop or start logging


using UnityEngine;
using System.Collections;
using Valve;
using Valve.VR;

//Our wrapper for the Vive. 
//Attach this to a GameObject that holds the Vive structure
using System.Collections.Generic;

namespace Libonati.VR{
	public class Vive_Controller : Singleton<Vive_Controller> {
		
		//Handle Loading Vive Data
		private bool loaded = false;
		public bool finishedLoading {
			get {
				return loaded;
			}
		}
		public delegate void OnFinishedLoading();
		private event OnFinishedLoading onFinishedLoading;

		[HideInInspector]
		public Vector3 roomSize;
		public GameObject roomBounds;
		private Mesh cubeMesh;

		private string[] buttonNames = System.Enum.GetNames (typeof(EVRButtonId));
		private System.Array buttonValues = System.Enum.GetValues (typeof(EVRButtonId));
		private List<int> possibleButtons;

		public Vive_Device head = new Vive_Device();
		public Vive_Device hand_L = new Vive_Device();
		public Vive_Device hand_R = new Vive_Device();


		public delegate void OnControllerButtonEvent(EVRButtonId buttonId, SteamVR_Controller.Device controller);
		public event OnControllerButtonEvent onControllerButtonDown;
		public event OnControllerButtonEvent onControllerButton;
		public event OnControllerButtonEvent onControllerButtonUp;
		public event OnControllerButtonEvent onControllerTouchDown;
		public event OnControllerButtonEvent onControllerTouch;
		public event OnControllerButtonEvent onControllerTouchUp;


		// Use this for initialization
		void Start () {
			StartCoroutine (initViveSetup());
		}

		//init all of the data we need to work with the Vive
		private IEnumerator initViveSetup(){

			//Wait until SteamVR is active
			bool canLoad = false;
			while (!canLoad) {
				if (!SteamVR.active){
					yield return new WaitForSeconds (2);
					continue;
				}
				canLoad = true;
			}
				
			//Save possible buttons
			possibleButtons = new List<int>();
			foreach (int value in buttonValues) {
				bool newValue = true;
				foreach (int oldItem in possibleButtons) {
					if (oldItem == value) {
						newValue = false;
					}
				}
				if (newValue) {
					possibleButtons.Add (value);
				}
			}

			//Create a debug Cube Mesh for drawing gizmos
			GameObject testCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cubeMesh = testCube.GetComponent<MeshFilter> ().mesh;
			Destroy (testCube);

			//Save room size
			float roomX = 1;
			float roomY = 1;
			float roomZ = 1;

			SteamVR_PlayArea playArea = gameObject.GetComponentInChildren<SteamVR_PlayArea> ();
			if (playArea != null) {
				roomY = playArea.wireframeHeight;
			}

			OpenVR.Chaperone.GetPlayAreaSize(ref roomX, ref roomZ);
			roomSize = new Vector3 (roomX, roomY ,roomZ);
			roomBounds.transform.localScale = roomSize;

			//LOAD HMD and CONTROLLERS
			head.controller = (Vive_Controller)this;
			hand_L.controller = (Vive_Controller)this;
			hand_R.controller = (Vive_Controller)this;

			#if LOG_DEBUG
			Debug.Log ("Loading Controllers");
			#endif

			SteamVR_ControllerManager controllerManager = GetComponentInChildren<SteamVR_ControllerManager> ();
			head.trackedObject = GetComponentInChildren<SteamVR_GameView> ().gameObject.GetComponent<SteamVR_TrackedObject> ();
			hand_L.trackedObject = controllerManager.left.GetComponent<SteamVR_TrackedObject> ();
			hand_R.trackedObject = controllerManager.right.GetComponent<SteamVR_TrackedObject> ();

			loaded = true;
			if (onFinishedLoading != null) {
				onFinishedLoading ();
			}
			yield return null;
		}
		public void addOnFinishedLoadingListener(OnFinishedLoading function){
			onFinishedLoading += function;
			if (finishedLoading) {
				onFinishedLoading ();
			}
		}

		//Draw debug information
		void OnDrawGizmos(){
			if (loaded) {
				//Draw Head
				if (head.data != null) {
					Gizmos.color = new Color (.1f, .7f, .5f);
					Gizmos.DrawSphere (head.positionLerp, .05f);
					Gizmos.DrawSphere (head.position, .1f);
					Gizmos.DrawLine (head.position, head.positionLerp);
				}

				//Draw Controllers
				if (hand_L.data != null) {
					Gizmos.color = new Color (.6f, .1f, .8f);//purple
					Gizmos.DrawSphere (hand_L.positionLerp, .025f);
					Gizmos.DrawSphere (hand_L.position, .05f);
					Gizmos.DrawLine (hand_L.position, hand_L.positionLerp);
				}

				if (hand_R.data != null) {
					Gizmos.color = new Color (.7f, .7f, .1f);//yellow
					Gizmos.DrawSphere (hand_R.positionLerp, .025f);
					Gizmos.DrawSphere (hand_R.position, .05f);
					Gizmos.DrawLine (hand_R.position, hand_R.positionLerp);
				}

				//Draw room
				Gizmos.color = new Color (0.1f, .8f, .1f, .5f);
					//Draw bounds
				Gizmos.DrawMesh (cubeMesh, transform.position,transform.rotation, new Vector3(roomSize.x, .01f, roomSize.z));
					//Draw forward
				Gizmos.color = new Color (0.6f, .2f, .5f, .5f);
				Gizmos.DrawMesh (cubeMesh, transform.position + transform.forward / 2, transform.rotation, new Vector3 (.1f, .1f, 1f));
			}
		}

		//Listen for button presses
		void Update(){
			if (!loaded) {
				return;
			}

			if(onControllerButton == null && onControllerButtonDown == null && onControllerButtonUp == null && onControllerTouch == null && onControllerTouchDown == null && onControllerTouchUp == null) {
				#if LOG_DEBUG
				Debug.Log("If you want to use Vive_Controllers button inputs then you must assign at least one button event by using onControllerButtonDown += myFunction");
				#endif
				return;
			}

			SteamVR_Controller.Device device = hand_L.data;
			for (int controllerI = 0; controllerI < 2; controllerI++) {
				if (device == null) {
					device = hand_R.data;
					continue;
				}
				foreach(int buttonId in possibleButtons){
					EVRButtonId button = (EVRButtonId)buttonId;
					//STANDARD BUTTON PRESSES
					if (device.GetPressDown (button)) {
						#if LOG_DEBUG
						Debug.Log("Device: " + controllerI + " BUTTON DOWN: " + button + ": " + buttonId);
						#endif
						if(onControllerButtonDown != null)
							onControllerButtonDown (button, device);
					}
					if (device.GetPressUp (button)) {
						#if LOG_DEBUG
						Debug.Log("Device: " + controllerI + " BUTTON UP: " + button + ": " +  buttonId);
						#endif
						if(onControllerButtonUp != null)
							onControllerButtonUp (button, device);
					}
					if (device.GetPress(button)) {
						#if LOG_DEBUG
						Debug.Log("Device: " + controllerI + " BUTTON HOLD: " + button + ": " +  buttonId);
						#endif
						if(onControllerButton != null)
							onControllerButton (button, device);
					}
				}
				//TOUCH DETECTION
				if (device.GetTouchDown (EVRButtonId.k_EButton_SteamVR_Touchpad)) {
					#if LOG_DEBUG
					Debug.Log("Device: " + controllerI + " TOUCH DOWN");
					#endif
					if(onControllerTouchDown != null)
						onControllerTouchDown (EVRButtonId.k_EButton_SteamVR_Touchpad, device);
				}
				if (device.GetTouchUp (EVRButtonId.k_EButton_SteamVR_Touchpad)) {
					#if LOG_DEBUG
					Debug.Log("Device: " + controllerI + " TOUCH UP");
					#endif
					if(onControllerTouchUp != null)
						onControllerTouchUp (EVRButtonId.k_EButton_SteamVR_Touchpad, device);
				}
				if (device.GetTouch(EVRButtonId.k_EButton_SteamVR_Touchpad)) {
					#if LOG_DEBUG
					Debug.Log("Device: " + controllerI + " TOUCH HOLD");
					#endif
					if(onControllerTouch != null)
						onControllerTouch (EVRButtonId.k_EButton_SteamVR_Touchpad, device);
				}
				device = hand_R.data;
			}
		}

		public Vector2 getTouchPadAxis(SteamVR_Controller.Device device){
			return device.GetAxis (EVRButtonId.k_EButton_SteamVR_Touchpad);
		}
		public float getTriggerAxis(SteamVR_Controller.Device device){
			return device.GetAxis (EVRButtonId.k_EButton_SteamVR_Trigger).x;
		}

		//Returns the distance from the closest hand
		public float getDistanceFromHand(Vector3 worldPosition){
			float distance1 = Vector3.Distance (hand_L.position, worldPosition);
			float distance2 = Vector3.Distance (hand_R.position, worldPosition);

			if (distance1 < distance2) {
				return distance1;
			} else {
				return distance2;
			}
		}
	}
}
